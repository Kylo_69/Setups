Tier    | Track
---     | ---
Superb  | Hungaroring, Kyalami, Silverstone, Spa-Francorchamps, Watkins Glen International
Great   | Autodromo Enzo e Dino Ferrari, Barcelona, Donington Park, Laguna Seca, Nurburgring, Oulton Park, Paul Ricard, Ricardo Tormo, Suzuka
Good    | Indianapolis Motor Speedway, Misano, Mount Panorama, Snetterton Circuit
Meh     | Circuit of the Americas, Monza, Zandvoort
Bin     | Brands Hatch, Zolder
