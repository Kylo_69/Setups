#!/bin/bash

# Ensure script is running in the current directory (Setups)
cd "$(dirname "$0")" || exit

# Run the check every 5 minutes indefinitely
while true; do
    # Check for changes in the Git working tree
    git fetch

    # Check if there are any changes (added, modified, deleted)
    CHANGES=$(git status --porcelain)

    if [ -n "$CHANGES" ]; then
        echo "Changes detected:"

        # Stage the changes for commit
        git add .

        # Loop through the changes and commit accordingly
        while IFS= read -r line; do
            STATUS=$(echo "$line" | awk '{print $1}') # Get the status (e.g., 'A', 'D', 'M')
            FILE=$(echo "$line" | awk '{print $2}')   # Get the file path

            # Extract car name and track name from file path
            CAR_NAME=$(echo "$FILE" | cut -d'/' -f1 | sed 's/_/ /g')
            TRACK_NAME=$(echo "$FILE" | cut -d'/' -f2 | sed 's/_/ /g')

            # Capitalize car name and track name properly (title case)
            CAR_NAME=$(echo "$CAR_NAME" | sed -r 's/\b./\U&/g')
            TRACK_NAME=$(echo "$TRACK_NAME" | sed -r 's/\b./\U&/g')

            case "$STATUS" in
                A) # File was added
                    COMMIT_MSG="added new setup for $CAR_NAME at $TRACK_NAME"
                    ;;
                D) # File was deleted
                    COMMIT_MSG="removed setup for $CAR_NAME at $TRACK_NAME"
                    ;;
                M) # File was modified
                    COMMIT_MSG="updated setup for $CAR_NAME at $TRACK_NAME"
                    ;;
                *) # Other statuses (untracked or renamed)
                    COMMIT_MSG="other change for $CAR_NAME at $TRACK_NAME"
                    ;;
            esac

            # Commit with the specific message
            git commit -m "$COMMIT_MSG"

        done <<< "$CHANGES"
    else
        echo "No changes detected."
    fi

    # Wait for 5 minutes before running the next check
    sleep 300

done
