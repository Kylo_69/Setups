# Porsche Setups

| Track	| No Setup, don't want to drive the track | Usable | Decent but not raced | Good & tested |
|:----------:|:----------:|:----------:|:----------:|:----------:|
| Barcelona     	|  |  |  |  |
| Brands Hatch    	| x |  |  |  |
| Circuit of The Americas    	|  |  |  |  |
| Donington    	|  |  | x |  |
| Hungaroring     	|  | x |  |  |
| Imola    	| x |  |  |  |
| Indianapolis    	|  |  |  |  |
| Kyalami    	| x |  |  |  |
| Laguna Seca    	|  | x |  |  |
| Misano    	|  |  |  |  |
| Monza    	|  |  |  |  |
| Mount Panorama    	|  |  |  |  |
| Nürburgring    	|  |  | x |  |
| Oulton Park    	|  |  |  |  |
| Paul Ricard    	|  | x |  |  |
| Ricardo Tormo    	|  | x |  |  |
| Silverstone    	|  |  |  |  |
| Snetterton    	|  | x |  |  |
| Spa    	| x |  |  |  |
| Suzuka    	|  |  | x |  |
| Watkins Glen    	|  |  |  |  |
| Zandvoort    	|  |  |  |  |
| Zolder    	| x |  |  |  |

# Ferrari Setups

| Track	| No Setup, don't want to drive the track | Usable | Decent but not raced | Good & tested |
|:----------:|:----------:|:----------:|:----------:|:----------:|
| Barcelona     	|  |  |  |  |
| Brands Hatch    	|  |  |  |  |
| Circuit of The Americas    	|  |  |  |  |
| Donington    	|  |  |  |  |
| Hungaroring     	|  |  |  |  |
| Imola    	|  |  |  |  |
| Indianapolis    	|  |  |  |  |
| Kyalami    	|  |  |  |  |
| Laguna Seca    	|  |  |  |  |
| Misano    	|  |  |  |  |
| Monza    	|  |  |  |  |
| Mount Panorama    	|  |  |  |  |
| Nürburgring    	|  |  |  |  |
| Oulton Park    	|  |  |  |  |
| Paul Ricard    	|  |  |  |  |
| Ricardo Tormo    	|  |  |  |  |
| Silverstone    	|  |  |  |  |
| Snetterton    	|  |  |  |  |
| Spa    	|  |  |  |  |
| Suzuka    	|  |  |  |  |
| Watkins Glen    	|  |  |  |  |
| Zandvoort    	|  |  |  |  |
| Zolder    	|  |  |  |  |

# Cayman Setups

| Track	| No Setup, don't want to drive the track | Usable | Decent but not raced | Good & tested |
|:----------:|:----------:|:----------:|:----------:|:----------:|
| Barcelona     	|  |  |  |  |
| Brands Hatch    	|  |  |  | x |
| Circuit of The Americas    	|  |  |  | x |
| Donington    	|  |  |  | x |
| Hungaroring     	|  |  |  |  |
| Imola    	|  |  |  |  |
| Indianapolis    	|  |  |  |  |
| Kyalami    	|  |  |  |  |
| Laguna Seca    	|  |  |  | x |
| Misano    	|  |  |  |  |
| Monza    	|  |  |  |  |
| Mount Panorama    	|  |  |  |  |
| Nürburgring    	|  |  |  |  |
| Oulton Park    	|  |  |  |  |
| Paul Ricard    	|  |  |  | x |
| Ricardo Tormo    	|  |  |  | x |
| Silverstone    	|  |  |  |  |
| Snetterton    	|  |  |  |  |
| Spa    	|  |  |  |  |
| Suzuka    	|  |  |  |  |
| Watkins Glen    	|  |  |  |  |
| Zandvoort    	|  |  |  |  |
| Zolder    	|  |  |  |  |