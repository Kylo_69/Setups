#!/bin/bash

rm output*.txt

files=("allan" "barrier" "foch" "geert" "honzik" "whitehead" "jimmy" "kargulu")

for file in "${files[@]}"
do
	echo "$file"
	python3 aor_t1.py "$file"
	python3 average.py "$file" > "output2_T1_$file.txt"
done

python3 aor_all_t1.py "$1" $2
